export interface IEmitterEvent {
    event: string;
    callback: () => void;
}

interface IEmitterHandler {
    emitter: IEmitter;
    events: IEmitterEvent[];
}

interface IEmitter {
    id: string;
    on(event: string | symbol, listener: (...args: any[]) => void): void;
    off(event: string | symbol, listener: (...args: any[]) => void): void;
    emit(event: string, ...args: any[]): void;
}

const PREDEFINED_EVENTS = ['disconnect', 'connect', 'QueueMatchMaking'];

export class SocketHandler {
    private emitHandlers: Map<string, IEmitterHandler>;

    constructor(private prefix: string) {
        this.emitHandlers = new Map();
    }

    public addEmitter(emitter: IEmitter) {
        if (this.emitHandlers.has(emitter.id)) {
            throw Error(`Emitter with id: ${emitter.id} is already added`);
        }

        this.emitHandlers.set(emitter.id, { emitter, events: [] });
    }

    public addEmitters(emitters: IEmitter[]) {
        emitters.forEach(emitter => {
            this.addEmitter(emitter);
        });
    }

    public removeEmitter(emitterId: string) {
        const emitHandler = this.emitHandlers.get(emitterId);
        if (!emitHandler) {
            throw Error(`Emitter with id: ${emitterId} is already removed`);
        }

        this.unregisterAllEvents(emitterId);
        this.emitHandlers.delete(emitterId);
    }

    public removeAllEmitters() {
        const keys = [...this.emitHandlers.keys()];
        for (const key of keys) {
            this.removeEmitter(key);
        }
    }

    public emit(event: string, data?: any) {
        const fullEventName = this.getFullEventName(event);
        this.emitHandlers.forEach(value => {
            value.emitter.emit(fullEventName, data);
        });
    }

    public registerEvent(emitterId: string, event: string, callback: () => void) {
        const fullEventName = this.getFullEventName(event);
        const foundEmitter = this.getEmitHandlerOrThrow(emitterId);

        if (!this.hasEvent(foundEmitter, fullEventName)) {
            foundEmitter.events.push({ event: fullEventName, callback });
            foundEmitter.emitter.on(fullEventName, callback);
        } else {
            throw Error(`event name: ${fullEventName} already registered`);
        }
    }

    public unregisterEvent(emitterId: string, event: string) {
        const fullEventName = this.getFullEventName(event);
        const foundEmitter = this.getEmitHandlerOrThrow(emitterId);

        const foundEvent = foundEmitter.events.find(e => e.event === fullEventName);
        if (foundEvent) {
            foundEmitter.events = foundEmitter.events.filter(e => e.event !== fullEventName);
            foundEmitter.emitter.off(fullEventName, foundEvent.callback);
        }
    }

    private unregisterAllEvents = (emitterId: string) => {
        const foundEmitter = this.getEmitHandlerOrThrow(emitterId);
        foundEmitter.events.forEach(event => {
            foundEmitter.emitter.off(event.event, event.callback);
        });

        foundEmitter.events = [];
    };

    private hasEvent = (emitHandler: IEmitterHandler, eventFullName: string) => {
        return emitHandler.events.some(e => e.event === eventFullName);
    };

    private getEmitHandlerOrThrow(emitterId: string) {
        const emitHandler = this.emitHandlers.get(emitterId);
        if (!emitHandler) {
            throw Error(`Could not find emit handler with id: ${emitterId}`);
        }
        return emitHandler;
    }

    private getFullEventName(event: string) {
        if (PREDEFINED_EVENTS.indexOf(event) >= 0) {
            return event;
        }

        return this.prefix + '-' + event;
    }
}
