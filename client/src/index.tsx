import React from 'react';
import ReactDOM from 'react-dom';
import { App } from './App';
// import { getSocket } from './socket';

// tslint:disable-next-line:no-var-requires
(window as any).PIXI = require('phaser-ce/build/custom/pixi');
// tslint:disable-next-line:no-var-requires
(window as any).p2 = require('phaser-ce/build/custom/p2');
// tslint:disable-next-line:no-var-requires
(window as any).Phaser = require('phaser-ce/build/custom/phaser-split');

if (module.hot) {
    module.hot.accept();
}

ReactDOM.render(<App />, document.getElementById('app'));

// getSocket();
