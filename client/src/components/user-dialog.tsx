import {
    Button,
    Dialog,
    DialogActions,
    DialogContent,
    DialogTitle,
    TextField,
} from '@material-ui/core';
import React from 'react';
import { AppContext } from '../context';

interface IUserDialogState {
    userName: string;
}

interface IUserDialogProps {
    setUserName(userName: string): void;
}

export class UserDialog extends React.Component<IUserDialogProps, IUserDialogState> {
    private readonly userNameMinLength = 3;

    constructor(props: IUserDialogProps) {
        super(props);
        this.state = {
            userName: '',
        };
    }

    public render() {
        return (
            <AppContext.Consumer>
                {context => (
                    <Dialog
                        open={!context.userName}
                        onClose={this.handleOnContinue}
                        aria-labelledby="form-dialog-title"
                    >
                        <DialogTitle id="form-dialog-title">Welcome to LunnNext</DialogTitle>
                        <DialogContent>
                            <TextField
                                autoFocus={true}
                                margin="dense"
                                id="name"
                                label="User name"
                                type="text"
                                fullWidth={true}
                                value={this.state.userName}
                                onChange={this.onUserNameChange}
                                helperText={`Minimum ${this.userNameMinLength} characters`}
                            />
                        </DialogContent>
                        <DialogActions>
                            <Button
                                onClick={this.handleOnContinue}
                                disabled={this.continueDisabled()}
                                color="primary"
                            >
                                Continue
                            </Button>
                        </DialogActions>
                    </Dialog>
                )}
            </AppContext.Consumer>
        );
    }

    private onUserNameChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        this.setState({ userName: e.currentTarget.value });
    };

    private getUserName = () => {
        return this.state.userName ? this.state.userName.trim() : '';
    };

    private continueDisabled = () => {
        return this.getUserName().length < this.userNameMinLength;
    };

    private handleOnContinue = () => {
        this.props.setUserName(this.getUserName());
    };
}
