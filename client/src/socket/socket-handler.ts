import socketIO from 'socket.io-client';

let socket: SocketIOClient.Socket | null = null;

function init() {
    const serverIP =
        process.env.NODE_ENV === 'production'
            ? 'https://server.lunne.nu'
            : `http://${window.location.hostname}:3000`;

    socket = socketIO(serverIP, {
        reconnection: true,
        reconnectionAttempts: 5,
        reconnectionDelay: 1000,
        reconnectionDelayMax: 5000,
    });
    return socket;
}

export function getSocket() {
    return socket && socket.connected ? socket : init();
}
