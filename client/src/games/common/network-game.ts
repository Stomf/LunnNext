import { SocketHandler } from 'shared/handlers';
import { getSocket } from 'src/socket';

export class NetworkGame {
    private socketHandler: SocketHandler;
    private socket: SocketIOClient.Socket | undefined;

    constructor(game: string) {
        this.socketHandler = new SocketHandler(game);
    }

    public connect = () => {
        this.socket = getSocket();
        this.attachSocketNetworkEvents();
    };

    public getConnected() {
        return this.socket && this.socket.id && this.socket.connected;
    }

    public getSocketId() {
        return this.socket && this.socket.id;
    }

    public emit(event: string, data?: any) {
        this.socketHandler.emit(event, data);
    }

    public registerEvent = (event: string, callback: (...args: any[]) => void) => {
        if (!this.socket) {
            throw Error('Socket not initialized');
        }

        this.socketHandler.registerEvent(this.socket.id, event, callback);
    };

    public unregisterEvent = (event: string) => {
        if (!this.socket) {
            throw Error('Socket not initialized');
        }

        this.socketHandler.unregisterEvent(this.socket.id, event);
    };

    public unregisterAllEvents = () => {
        if (!this.socket) {
            throw Error('Socket not initialized');
        }

        this.socketHandler.removeAllEmitters();
    };

    protected onConnected() {
        throw Error('Implement in sub-class');
    }

    private attachSocketNetworkEvents() {
        if (!this.socket) {
            throw Error('Socket not initialized');
        }

        this.socket.on('disconnect', this.onSocketDisconnect);
        this.socket.on('connect', this.onSocketConnect);

        if (this.socket.id && this.socket.connected) {
            this.onSocketConnect();
        }
    }

    private onSocketConnect = () => {
        if (!this.socket) {
            throw Error('Socket not initialized');
        }

        this.socketHandler.addEmitter(this.socket);
        this.onConnected();
    };

    private onSocketDisconnect = () => {
        this.socketHandler.removeAllEmitters();
    };
}
