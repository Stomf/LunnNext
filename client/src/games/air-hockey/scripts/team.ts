export const enum TeamSide {
    Left,
    Right,
}

export class Team {
    public score: number = 0;

    get TeamSide() {
        return this.teamSide;
    }

    constructor(private teamSide: TeamSide) {}

    public resetScore() {
        this.score = 0;
    }
}
