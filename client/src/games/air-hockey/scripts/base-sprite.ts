import { P2Sprite } from 'src/models';

export class BaseSprite {
    public sprite: P2Sprite;

    constructor(sprite: P2Sprite) {
        this.sprite = sprite;
    }

    public setDebug(debug: boolean) {
        this.sprite.body.debug = debug;
    }

    public setPosition(position: WebKitPoint) {
        this.sprite.body.x = position.x;
        this.sprite.body.y = position.y;
    }

    public getPosition() {
        return this.sprite.position;
    }

    public resetVelocity(velocityX?: number) {
        this.sprite.body.data.velocity = [velocityX ? velocityX : 0, 0];
    }
}
