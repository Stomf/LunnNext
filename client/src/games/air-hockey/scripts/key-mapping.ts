import Phaser from 'phaser-ce';

export interface IMapping {
    up: number;
    down: number;
    left: number;
    right: number;
    fire: number;
}

export const KeyMapping = {
    player1Mapping: {
        up: Phaser.Keyboard.W,
        down: Phaser.Keyboard.S,
        left: Phaser.Keyboard.A,
        right: Phaser.Keyboard.D,
        fire: Phaser.Keyboard.F,
    },
    player2Mapping: {
        up: Phaser.Keyboard.UP,
        down: Phaser.Keyboard.DOWN,
        left: Phaser.Keyboard.LEFT,
        right: Phaser.Keyboard.RIGHT,
        fire: Phaser.Keyboard.SPACEBAR,
    },
};
