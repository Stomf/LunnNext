import * as React from 'react';
import { AirHockeyGame } from './scripts/game';

// interface IAirHockeyProps extends RouteComponentProps<any> {}

class AirHockeyNetwork extends React.Component<any> {
    private game: AirHockeyGame | undefined;

    public render() {
        return <div id="AirHockeyCanvas" />;
    }

    public componentDidMount() {
        this.game = new AirHockeyGame('AirHockeyCanvas');
    }

    public componentWillUnmount() {
        if (this.game) {
            this.game.destroy();
        }
    }
}

export default AirHockeyNetwork;
