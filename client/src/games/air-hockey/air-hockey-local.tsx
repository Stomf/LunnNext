import * as React from 'react';
import { LocalAirHockeyGame } from './scripts/local-game';

class AirHockeyLocal extends React.Component<{}> {
    private game: LocalAirHockeyGame | undefined;

    public render() {
        return <div id="AirHockeyCanvas" />;
    }

    public componentDidMount() {
        this.game = new LocalAirHockeyGame('AirHockeyCanvas');
    }

    public componentWillUnmount() {
        if (this.game) {
            this.game.destroy();
        }
    }
}

export default AirHockeyLocal;
