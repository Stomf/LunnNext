import { Container } from 'inversify';
import getDecorators from 'inversify-inject-decorators';
import { UserService } from './services';

const container = new Container();
container.bind(UserService).to(UserService);

const { lazyInject } = getDecorators(container);

export { container, lazyInject };
