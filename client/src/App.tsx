import React from 'react';
import { UserDialog } from './components';
import { lazyInject } from './Container';
import { AppContext } from './context';
import AirHockeyNetwork from './games/air-hockey/air-hockey-network';
import { UserService } from './services';

interface IAppState {
    userName: string;
}

export class App extends React.Component<{}, IAppState> {
    @lazyInject(UserService)
    private userService!: UserService;

    constructor(props: {}) {
        super(props);
        this.state = {
            userName: '',
        };
    }

    public componentWillMount() {
        this.setState({ userName: this.userService.getUserName() });
    }

    public render() {
        return (
            <AppContext.Provider value={{ userName: this.state.userName }}>
                <UserDialog setUserName={this.setUserName} />
                <AirHockeyNetwork />
            </AppContext.Provider>
        );
    }

    private setUserName = (userName: string) => {
        this.userService.setUserName(userName);
        this.setState({ userName });
    };
}
