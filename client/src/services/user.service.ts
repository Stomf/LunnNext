import { injectable } from 'inversify';

@injectable()
export class UserService {
    private readonly USER_NAME_KEY = 'userName';

    public getUserName() {
        return localStorage.getItem(this.USER_NAME_KEY) || '';
    }

    public setUserName(userName: string) {
        localStorage.setItem(this.USER_NAME_KEY, userName);
    }
}
