import { Logger } from '@nestjs/common';
import {
    OnGatewayConnection,
    OnGatewayDisconnect,
    SubscribeMessage,
    WebSocketGateway,
    WsResponse,
} from '@nestjs/websockets';
import { OnSetUserNameRequest, OnSetUserNameResponse } from 'shared/interfaces/common';
import { Socket } from 'socket.io';
import { AppService } from './app.service';

@WebSocketGateway()
export class AppGateway implements OnGatewayConnection, OnGatewayDisconnect {
    constructor(private appService: AppService) {}

    public handleConnection(client: Socket) {
        Logger.log(`new connection client ${client.id}`);
        this.appService.addNewConnection(client);
    }

    public handleDisconnect(client: Socket) {
        Logger.log(`Disconnect client ${client.id}`);
        this.appService.removeConnection(client);
    }

    @SubscribeMessage('setUserName')
    protected onSetUserName(
        client: Socket,
        request: OnSetUserNameRequest
    ): WsResponse<OnSetUserNameResponse> {
        this.appService.setUserName(client, request);

        const event = 'setUserName';
        return { event, data: request };
    }
}
