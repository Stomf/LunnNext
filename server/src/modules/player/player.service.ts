import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreatePlayerDto, PlayerDto } from './dtos';
import { PlayerEntity } from './entities';

@Injectable()
export class PlayerService {
    constructor(
        @InjectRepository(PlayerEntity) private readonly playerRepository: Repository<PlayerEntity>
    ) {}

    public async create(createPlayerDto: CreatePlayerDto): Promise<PlayerDto> {
        const newPlayer = this.playerRepository.create({ name: createPlayerDto.name });
        const savedPlayer = await this.playerRepository.save(newPlayer);

        return this.toPlayerDto(savedPlayer);
    }

    public async findPlayer(id: number): Promise<PlayerDto | undefined> {
        const player = await this.playerRepository.findOne(id);

        if (player) {
            return this.toPlayerDto(player);
        }

        return undefined;
    }

    private toPlayerDto(entity: PlayerEntity): PlayerDto {
        return {
            id: entity.id,
            name: entity.name,
        };
    }
}
