import { Body, Controller, Get, HttpException, HttpStatus, Param, Post } from '@nestjs/common';
import { CreatePlayerDto, PlayerDto } from './dtos';
import { PlayerService } from './player.service';

@Controller('Player')
export class PlayerController {
    constructor(private readonly playerService: PlayerService) {}

    @Get(':id')
    public async GetPlayer(@Param() id: number): Promise<PlayerDto> {
        const player = await this.playerService.findPlayer(id);

        if (player) {
            return player;
        }

        throw new HttpException('Player was not found', HttpStatus.NOT_FOUND);
    }

    @Post()
    public async PostPlayer(@Body() createPlayerDto: CreatePlayerDto): Promise<PlayerDto> {
        return await this.playerService.create(createPlayerDto);
    }
}
