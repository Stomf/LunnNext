import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class PlayerEntity {
    @PrimaryGeneratedColumn()
    public id!: number;

    @Column({
        nullable: false,
        length: 30,
        unique: true,
    })
    public name!: string;
}
