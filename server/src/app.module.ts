import { Module } from '@nestjs/common';
import { AppGateway } from './app.gateway';
import { AppService } from './app.service';
import { MatchmakingModule } from './matchmaking/matchmaking.module';

@Module({
    imports: [MatchmakingModule],
    controllers: [],
    providers: [AppService, AppGateway],
})
export class AppModule {}
