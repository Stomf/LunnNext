import { Injectable } from '@nestjs/common';
import { Socket } from 'socket.io';

const DEFAULT_USER_NAME = 'Guest';

@Injectable()
export class AppService {
    public currentConnections: Map<string, string>;

    constructor() {
        this.currentConnections = new Map();
    }

    public addNewConnection(client: Socket) {
        this.currentConnections.set(client.id, DEFAULT_USER_NAME);
    }

    public removeConnection(client: Socket) {
        this.currentConnections.delete(client.id);
    }

    public setUserName(client: Socket, userName: string) {
        this.currentConnections.set(client.id, userName);
    }

    public getUserName(client: Socket) {
        return this.currentConnections.get(client.id) || DEFAULT_USER_NAME;
    }
}
