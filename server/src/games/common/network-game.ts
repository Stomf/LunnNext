import { SocketHandler } from 'shared/handlers';
import { Socket } from 'socket.io';

export class NetworkGame {
    protected socketHandler: SocketHandler;

    constructor(_gameId: string, gameName: string, clients: Socket[]) {
        this.socketHandler = new SocketHandler(gameName);
        this.socketHandler.addEmitters(clients);
    }

    public emitToPlayers = (event: string, data?: any) => {
        this.socketHandler.emit(event, data);
    };

    public registerEvent = (
        socket: Socket,
        eventName: string,
        callback: (...args: any[]) => void
    ) => {
        this.socketHandler.registerEvent(socket.id, eventName, callback);
    };

    public unregisterEvent = (socket: Socket, eventName: string) => {
        this.socketHandler.unregisterEvent(socket.id, eventName);
    };

    public removeAllEmitters = () => {
        this.socketHandler.removeAllEmitters();
    };
}
