import { ArgumentsHost, Catch, ExceptionFilter, Logger } from '@nestjs/common';

@Catch(Error)
export class AllErrorsFilter implements ExceptionFilter {
    public catch(exception: Error, _host: ArgumentsHost) {
        Logger.error(exception.message, exception.stack);
    }
}
