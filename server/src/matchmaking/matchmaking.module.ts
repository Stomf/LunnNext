import { Module } from '@nestjs/common';
import { MatchmakingGateway } from './matchmaking.gateway';
import { MatchmakingService } from './matchmaking.service';

@Module({
    imports: [],
    controllers: [],
    providers: [MatchmakingService, MatchmakingGateway],
})
export class MatchmakingModule {}
