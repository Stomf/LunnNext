import { Injectable, Logger } from '@nestjs/common';
import { LunnNet } from 'shared/interfaces';
import { Socket } from 'socket.io';
import { AirHockeyGame } from 'src/games/air-hockey/main';
import { MultiDictionary } from 'typescript-collections';
import { v4 } from 'uuid';

@Injectable()
export class MatchmakingService {
    private currentQueue: MultiDictionary<LunnNet.Game, Socket>;

    constructor() {
        this.currentQueue = new MultiDictionary<LunnNet.Game, Socket>();
    }

    public addToQueue(socket: Socket, game: LunnNet.Game) {
        Logger.log('Added player: ' + socket.id + ' to queue for game: ' + game);

        this.removeFromQueue(socket);
        this.currentQueue.setValue(game, socket);
        this.handleQueueChange(game);
    }

    public removeFromQueue(socket: Socket) {
        this.currentQueue.keys().forEach(game => {
            if (this.currentQueue.remove(game, socket)) {
                Logger.log('Removed player: ' + socket.id + ' from game: ' + game);
            }
        });
    }

    public handleQueueChange(game: LunnNet.Game) {
        const array = this.currentQueue.getValue(game);

        switch (game) {
            case 'AirHockey':
                while (array.length >= AirHockeyGame.MIN_PLAYERS) {
                    const playerOne = array.shift() as Socket;
                    const playerTwo = array.shift() as Socket;

                    this.currentQueue.remove(game, playerOne);
                    this.currentQueue.remove(game, playerTwo);

                    Logger.log('START!');

                    const airHockey = new AirHockeyGame(v4(), playerOne, playerTwo);
                    airHockey.initGame();
                }
                break;
            // case 'AchtungKurve':
            //     while (array.length >= AchtungKurveGame.MIN_PLAYERS) {
            //         const playerOne = array.shift() as Socket;
            //         const playerTwo = array.shift() as Socket;

            //         this.currentQueue.remove(game, playerOne);
            //         this.currentQueue.remove(game, playerTwo);

            //         const achtungKurve = new AchtungKurveGame([playerOne, playerTwo]);
            //         achtungKurve.initGame();
            //     }
            //     break;
            default:
                Logger.error(`Matchmaking - tried to queue for game: ${game}`);
                break;
        }
    }
}
