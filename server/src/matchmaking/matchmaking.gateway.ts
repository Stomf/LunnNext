import { Logger, UseFilters } from '@nestjs/common';
import {
    OnGatewayDisconnect,
    OnGatewayInit,
    SubscribeMessage,
    WebSocketGateway,
} from '@nestjs/websockets';
import { Matchmaking } from 'shared/interfaces/common';
import { Socket } from 'socket.io';
import { AllErrorsFilter } from 'src/errors';
import { MatchmakingService } from './matchmaking.service';

@UseFilters(new AllErrorsFilter())
@WebSocketGateway()
export class MatchmakingGateway implements OnGatewayDisconnect, OnGatewayInit {
    constructor(private matchMakingService: MatchmakingService) {}

    public afterInit(server: any) {
        Logger.log('server', server);
    }

    public handleDisconnect(client: Socket) {
        this.matchMakingService.removeFromQueue(client);
    }

    @SubscribeMessage('QueueMatchMaking')
    protected onQueueMatchMaking(client: Socket, request: Matchmaking.onQueueRequest) {
        this.matchMakingService.addToQueue(client, request.game);
    }
}
